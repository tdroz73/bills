use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::fmt::{self, Display, Formatter};

#[derive(Serialize, Deserialize, Debug)]
pub struct Bill {
    pub id: i32,
    pub payee: String,
    pub min_amount: f64,
    pub day_of_month_due: i32,
    pub use_second_checking: bool,
    pub url: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct BillPayment {
    pub id: i32,
    pub bill_id: i32,
    pub date_paid: DateTime<Utc>,
    pub amount: f64,
}

impl Display for Bill {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}:{}:{:.2}:{}:{}:{}",
            self.id,
            self.payee,
            self.min_amount,
            self.day_of_month_due,
            self.use_second_checking,
            self.url
        )
    }
}

impl Display for BillPayment {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}:{}:{}:{:.2}",
            self.id, self.bill_id, self.date_paid, self.amount
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bill_display() {
        let b = Bill {
            id: 1,
            payee: String::from("Someone"),
            min_amount: 123.50,
            day_of_month_due: 15,
            use_second_checking: true,
            url: String::from("https://www.billme.com/oh/yeah/baby"),
        };
        println!("{}", b);
    }

    #[test]
    fn test_bill_payment_display() {
        let b = BillPayment {
            amount: 123.50,
            bill_id: 1,
            date_paid: Utc::now(),
            id: 1,
        };
        println!("{}", b);
    }
}
