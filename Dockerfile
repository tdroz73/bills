FROM rust:latest

WORKDIR /usr/src/bills

COPY ./ ./

RUN cargo install --locked --path ./bills-service

CMD ["bills-service"]