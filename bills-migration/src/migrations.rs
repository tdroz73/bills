use barrel::backend::Pg;
use barrel::migration::Migration;
use barrel::types;
use postgres::{Client, Error};

pub trait BillsMigration {
    fn new() -> Self;
    fn run(&self, pg_client: &mut Client) -> Result<u64, postgres::Error>;
}

pub struct CreateBillsTableMigration {}
impl BillsMigration for CreateBillsTableMigration {
    fn new() -> Self {
        CreateBillsTableMigration {}
    }

    fn run(&self, pg_client: &mut Client) -> Result<u64, Error> {
        let mut m = Migration::new();
        m.create_table_if_not_exists("bills", |t| {
            t.add_column("id", types::integer().primary(true).increments(true));
            t.add_column("payee", types::varchar(255).nullable(false));
            t.add_column("min_amount", types::float().nullable(false));
            t.add_column("day_of_month_due", types::integer().nullable(false));
            t.add_column("use_second_checking", types::boolean().nullable(false));
            t.add_column("url", types::text().nullable(false));
        });
        let bills_table = m.make::<Pg>().to_owned();
        println!("Table {} will be created!", bills_table);
        pg_client.execute(&bills_table[..], &[])
    }
}

pub struct CreateBillPaymentTableMigration {}
impl BillsMigration for CreateBillPaymentTableMigration {
    fn new() -> Self {
        CreateBillPaymentTableMigration {}
    }

    fn run(&self, pg_client: &mut Client) -> Result<u64, Error> {
        let mut m = Migration::new();
        m.create_table_if_not_exists("bill_payments", |t| {
            t.add_column("id", types::integer().primary(true).increments(true));
            t.add_column("bill_id", types::integer().nullable(false));
            t.add_column("date_paid", types::date().nullable(false));
        });

        let bill_payments_table = m.make::<Pg>().to_owned();
        println!("Table {} will be created!", bill_payments_table);
        pg_client.execute(&bill_payments_table[..], &[])
    }
}
