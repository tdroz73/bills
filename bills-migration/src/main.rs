use postgres::tls::NoTls;
use postgres::Client;

use std::env;

mod migrations;
use migrations::*;

fn main() -> Result<(), postgres::Error> {
    println!("Starting migration ....");
    // example - postgres://postgres:docker@172.17.0.2:5432/postgres
    let db_url = env::var("DB_URL").unwrap();
    println!("Using db: {}", db_url);
    let mut client = Client::connect(&db_url, NoTls)?;
    println!("Client created - starting actual migration...");
    let migrations = vec![
        CreateBillsTableMigration::new().run(&mut client),
        CreateBillPaymentTableMigration::new().run(&mut client),
    ];

    for result in migrations.iter() {
        match result {
            Ok(changes) => print!("Migration Success: {} \n", changes),
            Err(error) => print!("Migration Failure: {} \n", error),
        };
    }

    Ok(())
}
