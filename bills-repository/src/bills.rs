use bills_contract::Bill;
use tokio_postgres::types::ToSql;
use tokio_postgres::{Client, Row};

// TODO - address the unwrap and handle appropriately

pub async fn get_by_id(id: &i32) -> Option<Bill> {
    let client: Client = super::connect().await.unwrap();
    let row: &Row = &client
        .query_one("select id, payee, min_amount, day_of_month_due, use_second_checking, url from bills where id=$1", &[&id])
        .await
        .unwrap();
    return Some(map_row(&row));
}

pub async fn insert(bill: &Bill) -> bool {
    let client: Client = super::connect().await.unwrap();
    let values: [&'_ (dyn ToSql + Sync); 5] = extract_values(&bill);
    let _num: u64 = client
        .execute("INSERT INTO bills (payee, min_amount, day_of_month_due, use_second_checking, url) VALUES ($1, $2, $3, $4, $5)", &values)
        .await
        .unwrap();
    return _num > 0;
}

pub async fn delete(id: &i32) -> bool {
    let client: Client = super::connect().await.unwrap();
    let _num: u64 = client
        .execute("DELETE FROM bills where id = $1", &[&id])
        .await
        .unwrap();
    return _num == 1;
}

pub async fn list_all() -> Option<Vec<Bill>> {
    let client: Client = super::connect().await.unwrap();
    let mut vec_bills: Vec<Bill> = Vec::new();
    let rows: Vec<Row> = client
        .query(
            "SELECT id, payee, min_amount, day_of_month_due, use_second_checking, url from bills",
            &[],
        )
        .await
        .unwrap();
    for row in rows {
        vec_bills.push(map_row(&row));
    }
    return Some(vec_bills);
}

pub async fn update(bill: &Bill) -> bool {
    let client: Client = super::connect().await.unwrap();
    let _values: [&'_ (dyn ToSql + Sync); 6] = extract_values_with_id(bill);
    let _num: u64 = client
        .execute("UPDATE bills SET payee = $1, min_amount = $2, day_of_month_due = $3, use_second_checking = $4, url = $5 where id = $6", &_values)
        .await
        .unwrap();
    return _num == 1;
}

fn map_row(row: &Row) -> Bill {
    let bill = Bill {
        id: row.get(0),
        payee: row.get(1),
        min_amount: row.get(2),
        day_of_month_due: row.get(3),
        use_second_checking: row.get(4),
        url: row.get(5),
    };
    return bill;
}

fn extract_values(bill: &Bill) -> [&'_ (dyn ToSql + Sync); 5] {
    let values: [&'_ (dyn ToSql + Sync); 5] = [
        &bill.payee,
        &bill.min_amount,
        &bill.day_of_month_due,
        &bill.use_second_checking,
        &bill.url,
    ];
    return values;
}

fn extract_values_with_id(bill: &Bill) -> [&'_ (dyn ToSql + Sync); 6] {
    let values: [&'_ (dyn ToSql + Sync); 6] = [
        &bill.payee,
        &bill.min_amount,
        &bill.day_of_month_due,
        &bill.use_second_checking,
        &bill.url,
        &bill.id,
    ];
    return values;
}
