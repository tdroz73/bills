pub mod bill_payments;
pub mod bills;

// TODO - change this to use R2D2 for connection poll mgmt

use tokio;
use tokio_postgres::NoTls;

use std::env;

pub async fn connect() -> Option<tokio_postgres::Client> {
    let connect_params = env::var("TOKIO_DB_HOST").unwrap();
    let (client, conn) = tokio_postgres::connect(&connect_params, NoTls)
        .await
        .unwrap();
    tokio::spawn(async move {
        if let Err(e) = conn.await {
            eprintln!("connection error: {}", e);
        }
    });
    return Some(client);
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
