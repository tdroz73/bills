use bills_contract::BillPayment;

pub async fn get_bill_payment_by_id(id: &u32) -> Option<BillPayment> {
    let client = super::connect().await.unwrap();
    let rows = &client
        .query("select * from bill_payments where id=$1", &[&id])
        .await
        .unwrap();
    let row = rows.get(0).unwrap();
    let bill_payment = BillPayment {
        id: row.get(0),
        bill_id: row.get(1),
        date_paid: row.get(2),
        amount: row.get(3),
    };
    return Some(bill_payment);
}
