use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use bills_contract::Bill;

#[path = "bill_service.rs"]
mod bill_service;

#[get("/")]
pub async fn index() -> impl Responder {
    format!("Endpoints: /api/v1/bills")
}

#[get("/api/v1/bills")]
pub async fn list_bills() -> HttpResponse {
    let bills = bill_service::list_all().await;
    HttpResponse::Ok().json(bills)
}

#[get("/api/v1/bills/{id}")]
pub async fn get_bill_by_id(info: web::Path<i32>) -> HttpResponse {
    let id = info.as_ref();
    let bill = bill_service::get_by_id(id).await;
    HttpResponse::Ok().json(bill)
}

#[delete("/api/v1/bills/{id}")]
pub async fn delete_bill(info: web::Path<i32>) -> HttpResponse {
    let id = info.as_ref();
    let deleted = bill_service::delete(id).await;
    return if deleted {
        HttpResponse::Ok().body("Successfully deleted.")
    } else {
        HttpResponse::NotFound().body("Could not find anything with that id.")
    };
}

#[post("/api/v1/bills")]
pub async fn create_bill(bill: web::Json<Bill>) -> HttpResponse {
    let results = bill_service::create(&bill).await;
    return if results {
        HttpResponse::Ok().json(bill.into_inner())
    } else {
        HttpResponse::InternalServerError().body("There was a problem creating the record.")
    };
}

#[put("/api/v1/bills/{id}")]
pub async fn update_bill(bill: web::Json<Bill>) -> HttpResponse {
    let results = bill_service::update(&bill).await;
    return if results {
        HttpResponse::Ok().json(bill.into_inner())
    } else {
        HttpResponse::InternalServerError().body("There was a problem updating the record.")
    };
}
