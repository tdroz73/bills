mod endpoints;

use actix_web::{App, HttpServer};
use endpoints::*;
use log::*;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_server=info,actix_web=info");
    env_logger::init();

    info!("Rust Actix Server running... http://0.0.0.0:8080");
    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(create_bill)
            .service(delete_bill)
            .service(get_bill_by_id)
            .service(list_bills)
            .service(update_bill)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
