use bills_contract::Bill;
use bills_repository::bills;

pub async fn get_by_id(id: &i32) -> Option<Bill> {
    return bills::get_by_id(id).await;
}

pub async fn create(bill: &Bill) -> bool {
    return bills::insert(bill).await;
}

pub async fn delete(id: &i32) -> bool {
    return bills::delete(id).await;
}

pub async fn list_all() -> Option<Vec<Bill>> {
    return bills::list_all().await;
}

pub async fn update(bill: &Bill) -> bool {
    return bills::update(bill).await;
}
